<?php
/**
 * sklik api class
 * provides comunication with sklik api
 * api url: http://api.sklik.cz/
 * @author Michal Haták <me@twista.cz>
 */


namespace Twista\Sklik;

class Api {

    /** @var array */
    protected $config;

    /** @var \Twista\XMLRPC */
    protected $client = null;

    const SESSION_IDENTIFICATOR = 'twista.sklik';

    const API_URL = 'https://api.sklik.cz/bajaja/RPC2';

    const SANDBOX_URL = 'https://api.sklik.cz/sandbox/bajaja/RPC2';

    /** @var  array|null */
    protected $lastError;

    /**
     * constructor
     * usage:
     * $config = array(
     *    'username' => '...',
     *    'password' => '...',
     *    'sandbox' => true, // not required, just for sandbox
     * );
     * @param array $config
     */
    public function __construct($config) {
        $this->config = $config; // TODO: is that necessary ?

        // resolve sandbox
        if (isset($config['sandbox']) && $config['sandbox'] === true) {
            $this->client = new \Twista\XMLRPC(self::SANDBOX_URL);
        } else {
            $this->client = new \Twista\XMLRPC(self::API_URL);
        }
        $this->session = null; //& $_SESSION[self::SESSION_IDENTIFICATOR];

        $this->login($config['username'], $config['password']);
    }


    /**
     * login into sklik api
     * @param string $username
     * @param string $password
     * @return bool
     */
    public function login($username, $password) {
        $data = $this->call('client.login', array($username, $password));
        if ($data !== false) {
            return true;
        }
        return false;
    }

    /**
     * list campaigns
     * @param null $id
     * @return Campaign[]|bool
     */
    public function getCampaigns($id = null) {
        $data = $this->call('listCampaigns');
        if ($data !== false) {
            $result = array();
            foreach ($data['campaigns'] as $campaign) {
                if ((!is_null($id)) && ($campaign['id'] == $id))
                    return new Campaign($campaign);
                else
                    $result[] = new Campaign($campaign);
            }
            if (!is_null($id))
                return false;
            return $result;
        }
        return false;
    }


    /**
     * =========== CREATE ==========
     */

    /**
     * @param Object $object
     * @return bool|mixed|null
     */
    public function create(Object $object) {
        if ($object instanceof Campaign) {
            return $this->createCampaign($object);
        }
        return null; // TODO: catch all types, not implemented yet
    }

    /**
     * create campaign
     * @param Campaign $campaign
     * @return bool|mixed
     */
    public function createCampaign(Campaign $campaign) {
        $data = $this->call('campaign.create', array($campaign->toArray()));
        return ($data) ? true : false;
    }


    /**
     * =========== CHECK ==========
     */

    /**
     * check, factory for create<Element>
     * @param Object $object
     * @return bool|mixed|null
     */
    public function check(Object $object) {
        if ($object instanceof Campaign) {
            return $this->checkCampaign($object);
        }

        return null; // TODO: catch all types, not implemented yet
    }

    /**
     * check if campaigns is properly defined
     * @param Campaign $campaign
     * @return bool
     */
    public function checkCampaign(Campaign $campaign) {
        $campaign = $campaign->toArray();
        if (empty($campaign) || !isset($campaign['name']))
            throw new \InvalidArgumentException("Campaing must contains name");
        $data = $this->call('campaign.checkAttributes', array($campaign));
        return ($data) ? true : false;
    }


    /**
     * =========== DELETE ==========
     */

    public function delete(Object $object) {
        if ($object instanceof Campaign) {
            return $this->deleteCampaign($object);
        }

        return null;
    }

    public function deleteCampaign($campaign) {
        if ($campaign instanceof Campaign) {
            $campaign = $campaign->id;
        } else if (!is_int($campaign)) {
            throw new \InvalidArgumentException("Parametr 1 must be instance of Campaign or integer, " . gettype($campaign) . " given.");
        }

        $data = $this->call('campaign.remove', array($campaign));
        return ($data) ? true : false;
    }

    /**
     * restore object
     * @param $object
     * @return bool
     */
    public function restore($object) {
        if ($object instanceof Campaign) {
            return $this->restoreCampaign($object);
        }

        return false;
    }

    /**
     * restore campaign
     * @param Campaign|int $campaign
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function restoreCampaign($campaign) {
        if ($campaign instanceof Campaign) {
            $campaign = $campaign->id;
        } else if (!is_int($campaign)) {
            throw new \InvalidArgumentException("Parametr 1 must be instance of Campaign or integer, " . gettype($campaign) . " given.");
        }

        $data = $this->call('campaign.restore', array($campaign));
        return ($data) ? true : false;

    }

    /**
     * returns array of search services
     * @return array|bool
     */
    public function getSearchServices() {
        $data = $this->call('listSearchServices');
        if ($data) {
            return $data['searchServices'];
        }
        return false;
    }

    /**
     * returns array of regions
     * @return array|bool
     */
    public function getRegions() {
        $data = $this->call('listPredefinedRegions');
        if ($data) {
            return $data['predefinedRegions'];
        }
        return false;
    }

    /**
     * do a api call via XMLRPC
     * @param string $method method name
     * @param array|null $params
     * @return bool|mixed
     */
    protected function call($method, $params = array()) {
        if (!is_null($this->session))
            array_unshift($params, $this->session);

        $response = $this->client->send($method, $params);

        if ($response['status'] == 200) {
            $this->lastError = null;
            if (!is_null($params))
                $this->session = $response['session'];
            return $response;
        } else {
            $this->lastError = $response;
            $this->error($response, $method, $params);
            return false;
        }

    }

    /**
     * return last error
     * @return array|null
     */
    public function getLastError() {
        return $this->lastError;
    }

    /**
     * error handler
     * TODO make it better :D
     */
    public function error($response, $method, $params) {
        throw new \Exception(print_r($response, true), $response['status']);
        echo sprintf("gets error calling method %s <br /> response [%d]:%s,: ", $method, $response['status'], $response['statusMessage']);
        var_dump($response);
        var_dump($response);

    }

    /**
     * logout
     * @return bool
     */
    public function logout() {
        return ($this->call('client.logout')) ? true : false;
    }
}