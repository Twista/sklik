<?php
/**
 * sklik conversion object
 * @author Michal Haták <me@twista.cz>
 */
namespace Twista\Sklik;

class Conversion extends Object {
    /** @var string */
    protected $name;

    /** @var  int */
    protected $conversionTypeId;

    /** @var  string */
    protected $proto;

    /** @var  string */
    protected $color;

    /** @var  int */
    protected $value;

}