Sklik api class
====================

this class provides functionality of sklik api

Installation
====================
Recommended installation is via composer, but you can simple download zip release
....

Usage:
====================

    $config = array(
        'username' => 'your_mail@seznam.cz',
        'password' => 'password',
    );

    $api = new Twista\Sklik\Api($config);

list campaigns

    $campaigns = $api->getCampaigns();

    foreach($campaigns as $campaign){
        echo $campaign->name . '<br />';
    }

list search services

    var_dump($api->getSearchServices());

list predefined regions

    var_dump($api->getRegions());

create campaign

    $campaign = new Twista\Sklik\Campaign(array(
        'name' => 'Test campaign name',
        'dayBudget' => 10000,
        'excludedSearchServices' => array(
            1
        ),
        'excludedUrls' => array('twista.cz'),
        'totalBudget' => 10000,
        'totalClicks' => 10000));

    // check if campaign is defined properly
    $api->check($campaign); // alias for $api->checkCampaign($campaign);
    // create campaign
    $api->create($campaign); // alias for $api->createCampaign($campaign);



Tests
====================

to run tests create file config.test.php in /tests directory,

file looks like

    <?php

    return array(
        'username' => 'your_username',
        'password' => 'your_password',
    );

insert your data to sklik account a feel free to run tests (tests run in sandbox mode, so dont care about your data)
