<?php
/**
 * sklik ClientStats object
 * @author Michal Haták <me@twista.cz>
 */
namespace Twista\Sklik;


class ClientStats extends Object {

    /** @var array */
    protected $stats = array(
        'avgPosition' => null, // float
        'clicks' => null, // int
        'impressions' => null, // int
        'conversions' => null, // int
        'transactions' => null, // int
        'value' => null, // int
        'money' => null, // int
    );

    /** @var array */
    protected $fulltext = array(
        'avgPosition' => null, // float
        'clicks' => null, // int
        'impressions' => null, // int
        'conversions' => null, // int
        'transactions' => null, // int
        'value' => null, // int
        'money' => null, // int
    );

    /** @var array */
    protected $context = array(
        'avgPosition' => null, // float
        'clicks' => null, // int
        'impressions' => null, // int
        'conversions' => null, // int
        'transactions' => null, // int
        'value' => null, // int
        'money' => null, // int
    );
}