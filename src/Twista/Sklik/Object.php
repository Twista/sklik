<?php

namespace Twista\Sklik;


abstract class Object {

    /**
     * constructor
     * allows set just defined properties
     * @param $data
     * @throws MemberAccessException
     */
    public function __construct($data) {
        foreach ($data as $property => $value) {
            if (!property_exists($this, $property))
                throw new MemberAccessException("Object haven't property {$property}");
            $this->{$property} = $value;
        }
    }

    /**
     * magic function get
     * allows read defined properties
     * @param string $name
     * @return mixed
     * @throws MemberAccessException
     */
    public function __get($name) {
        if (property_exists($this, $name))
            return $this->{$name};
        throw new MemberAccessException("Undefined property {$name}.");
    }

    /**
     * get no-null object properties as array
     * @return array
     */
    public function toArray() {
        $ret = array();
        foreach (get_object_vars($this) as $key => $value) {
            if (!is_null($value))
                $ret[$key] = $value;
        }
        return $ret;
    }
}