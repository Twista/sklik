<?php
/**
 * sklik campaign object
 * @author Michal Haták <me@twista.cz>
 */
namespace Twista\Sklik;

class Campaign extends Object {

    /** @var int */
    protected $id;

    /** @var string */
    protected $name;

    /** @var  bool */
    protected $removed;

    /** @var  string ['active','suspend'] */
    protected $status;

    /** @var  int */
    protected $dayBudget;

    /** @var  int */
    protected $exhaustedDayBudget;

    /** @var  string ['weighted','random'] */
    protected $adSelection;

    /** @var  \DateTime */
    protected $startDate;

    /** @var  \DateTime */
    protected $endDate;

    /** @var  \DateTime */
    protected $createDate;

    /** @var  bool */
    protected $context;

    /** @var  int[] */
    protected $excludedSearchServices;

    /** @var  string[] */
    protected $excludedUrls;

    /**
     * @var  array[]
     * contains arrays > array('name' => '..', 'matchType' => type) where type = ['negativeBroad','negativePhrase','negativeExact']
     */
    protected $negativeKeywords;

    /** @var  int */
    protected $userId;

    /** @var  int */
    protected $totalBudget;

    /** @var  int */
    protected $exhaustedTotalBudget;

    /** @var  int */
    protected $totalClicks;

    /** @var  int */
    protected $exhaustedTotalClicks;

    /**
     * @var array[]
     * contains arrays >
     * array > (
     *   'type' => type, // ['predefined','circle','polygon']
     *   'predefinedId' => int,
     *   'latitude' => float,
     *   'longitude' => float,
     *   'radius' => int,
     *   'vertices' => array(
     *      array('latitude' => float, 'longitude' => float)
     *      ),
     *
     * )
     */
    protected $regions;

    /** @var  int */
    protected $premiseId;


}