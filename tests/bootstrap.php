<?php

include __DIR__ . '/../vendor/autoload.php';

class_alias('Tester\Assert', 'Assert');
date_default_timezone_set('Europe/Prague');

function run(Tester\TestCase $testCase) {
    $testCase->run(isset($_SERVER['argv'][1]) ? $_SERVER['argv'][1] : NULL);
}

$_SESSION = array();

$config = include(__DIR__ . '/config.test.php');

Tester\Environment::setup();
